#!/usr/bin/python3

import argparse

def parseargs():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='This python script writes a constant stream of dummy uuid data to postgresql for the desired amount of time',
        epilog='''
All options:
  -d, --database        The database with which you wish to connect
  -n, --hostname        The hostname or IP of the postgresql database
  -l, --length          The length you want the dummy inserter to run (in seconds)
  -p, --password        The password of the connection user
  -t, --table           The database table onto which you wish to write data
  -u, --user            The user with which to connect

Example:
  ./app.py --host 127.0.0.1 --database postgres --user postgres --password postgres --table test --length 120
  ''')
    parser.add_argument('-d','--database', dest='database', default='dummy', type=str, help='The database with which you wish to connect')
    parser.add_argument('-n','--hostname', dest='hostname', default='127.0.0.1', type=str, help='The hostname or IP of the postgresql database')
    parser.add_argument('-l','--length', dest='length', default='1', type=int, help=' The length you want the dummy inserter to run')
    parser.add_argument('-p','--password', dest='password', default='postgres', type=str, help='The password of the connection user')
    parser.add_argument('-t','--table', dest='table', default='dummy', type=str, help=' The database table onto which you wish to write data')
    parser.add_argument('-u','--user', dest='user', default='postgres', type=str, help='The user with which to connect')
    return parser.parse_args()
