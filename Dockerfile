from python:3.7-stretch

MAINTAINER carlos.robles@gmail.com

COPY Pipfile Pipfile.lock /app/

WORKDIR /app

RUN apt-get update -y && \
    apt-get install -y netcat && \
    pip install --upgrade pip && \
    pip install pipenv && \
    pipenv install --deploy --system

COPY app.py /app/

COPY utils/ /app/utils/

CMD ["python", "/app/app.py", "--host", "db", "--length", "86400"]
