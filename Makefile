down:
	docker-compose down

build: down
	docker-compose build

up: down build
	docker-compose up -d

.PHONY: down build up
