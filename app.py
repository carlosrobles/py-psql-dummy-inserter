#!/usr/bin/python3

from psycopg2 import connect
from psycopg2 import sql
from utils.parseargs import parseargs

import time
import uuid

# Generate current time in epoch milliseconds
current_milli_time = lambda: int(round(time.time() * 1000))

# Generate a random 32 character uuid
gen_uid = lambda: (uuid.uuid4()).hex


def connect_db(database):
    """
    Takes input database and try to connect to it.
    Attempts to connect to the database for 60 seconds
    before giving up.
    Returns a connection and cursor object.
    """
    try:
        connection = connect(dbname=database,
                             user=args.user,
                             host=args.hostname,
                             password=args.password)
        cursor = connection.cursor()
        print("successfully connected to db {}".format(database))
        return connection, cursor
    except Exception as e:
        print("error connecting to database %s: %s" % (database, e))
        return None


def create_database(database):
    """
    Takes input database and create it.
    """
    connection, cursor = connect_db('postgres')
    connection.autocommit = True
    try:
        cursor.execute('CREATE DATABASE ' + database)
        print("successfully created db {}".format(database))
    except Exception as e:
        print("error creating database %s: %s" % (database, e))
    cursor.close()
    connection.close()


def create_table(database, table):
    """
    Takes input database and table.
    Creates table in database.
    """
    connection, cursor = connect_db(database)
    connection.autocommit = True
    try:
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS {} (
                id SERIAL PRIMARY KEY,
                epochmilli bigint,
                uuid uuid,
                uuid2 uuid,
                uuid3 uuid,
                uuid4 uuid,
                uuid5 uuid,
                uuid6 uuid,
                uuid7 uuid,
                uuid8 uuid,
                uuid9 uuid,
                uuid10 uuid);
            """.format(table)
            )
        print("successfully created table {}".format(table))
    except Exception as e:
        print("error creating database table %s: %s" % (table, e))
    cursor.close()
    connection.close()


def write_data(database, table):
    """
    Takes input database and table.
    Writes a constant stream of random uuid to data table.
    """
    connection, cursor = connect_db(database)
    connection.autocommit = True
    end_time = time.time() + args.length
    message_number = 0
    while time.time() < end_time:
        current_time = current_milli_time()
        try:
            cursor.execute(
                sql.SQL(
                    """
                    INSERT INTO {} (
                        epochmilli,
                        uuid,
                        uuid2,
                        uuid3,
                        uuid4,
                        uuid5,
                        uuid6,
                        uuid7,
                        uuid8,
                        uuid9,
                        uuid10
                    )
                    VALUES (
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s,
                        %s
                    )
                    """
                ).format(sql.Identifier(table)), [
                    current_time,
                    gen_uid(),
                    gen_uid(),
                    gen_uid(),
                    gen_uid(),
                    gen_uid(),
                    gen_uid(),
                    gen_uid(),
                    gen_uid(),
                    gen_uid(),
                    gen_uid()
                ]
            )
        except Exception as e:
            print("error inserting into table %s: %s" % (table, e))
        message_number += 1
        print("%s - committed message #%s" % (current_time, str(message_number)))
    cursor.close()
    connection.close()


# Write random dummy data to our target psql db
if __name__ == "__main__":
    args = parseargs()
    create_database(args.database)
    create_table(args.database, args.table)
    write_data(args.database, args.table)
